# Auto Tap Heroes Clicker

Auto click through the clicker game 'Tap Heroes'. This auto clicker script attack monsters. collect gold, upgrade stats and cast spells.

## Requirements

- Windows operating system
- [AutoIt v3](https://www.autoitscript.com/site/) installed

## How to use

1. Run this clicker application script `"C:\Program Files (x86)\AutoIt3\AutoIt3.exe" Auto-Tap-Heroes-Clicker.au3`.
2. Close the dailog that confirms that the script started.
3. Open 'Tap Heroes' from Steam, the clicker script will start clicking through the game.
4. Press the 'Esc' key to stop the script's execution earlier than the specified amount of clicks in the script.

## Tools used for Development

- Click automation developed with the [AutoIt3][autoit] scripting language.
- GUI Prototyping with [Pencil][pencil].
- The GUI implementation done with [Koda Form Designer][koda].

[autoit]: https://www.autoitscript.com/site/
[pencil]: https://pencil.evolus.vn/
[koda]: http://koda.darkhost.ru/page.php?id=index
