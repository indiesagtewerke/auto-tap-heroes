#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         Quintin Henn

 Script Function:
	Automate the idle clicker game Tap Heroes.

#ce ----------------------------------------------------------------------------

#include <AutoItConstants.au3>
#include <MsgBoxConstants.au3>
#include <Misc.au3>

Const $appName = "Auto Tap Heroes Clicker"

Const $clicks = 1000000
Const $collectGoldMark = 100 ;~ 500
Const $nextLevelMark = 2000
Const $upgradeMark = 800
Const $spellMark = 1000

Global $doCollectGold = true
Global $doGoNextLevel = false
Global $doUpgrade = true
Global $doCastSpells = true

Global $escapeKeyCode = "1B"
Global $hDLL = DllOpen("user32.dll")

Global Enum $attackUpgrade, $healUpgrade, $criticalUpgrade
Global Enum $handOfGodSpell, $goldRushSpell, $greatTideSpell, $battleCry, $healingLight, $arrowToTheKnee

Func AttackMonster()
  MouseClick($MOUSE_CLICK_PRIMARY, 1060, 570, 1, 0)
EndFunc   ;==>AttackMonster

Func CollectGold()
  MouseMove(1055, 600, 10)
  MouseMove(800, 600, 50)
EndFunc   ;==>CollectGold

Func GotoNextLevel()
  MouseClick($MOUSE_CLICK_PRIMARY, 1170, 530, 1, 0)
EndFunc   ;==>GotoNextLevel

Func Upgrade($upgradeParam)
  Select
    Case $upgradeParam = $attackUpgrade
      MouseClick($MOUSE_CLICK_PRIMARY, 880, 680, 1, 0)
    Case $upgradeParam = $healUpgrade
      MouseClick($MOUSE_CLICK_PRIMARY, 960, 680, 1, 0)
    Case $upgradeParam = $criticalUpgrade
      MouseClick($MOUSE_CLICK_PRIMARY, 1040, 680, 1, 0)
    Case Else
      Upgrade(Random(0, 2, 1))
  EndSelect
EndFunc   ;==>Upgrade

Func CastSpell($spellParam)
  Select
    Case $spellParam = $handOfGodSpell
      MouseClick($MOUSE_CLICK_PRIMARY, 720, 680, 1, 0)
    Case $spellParam = $goldRushSpell
      MouseClick($MOUSE_CLICK_PRIMARY, 770, 680, 1, 0)
    Case $spellParam = $greatTideSpell
      MouseClick($MOUSE_CLICK_PRIMARY, 820, 680, 1, 0)
    Case $spellParam = $battleCry
      MouseClick($MOUSE_CLICK_PRIMARY, 1090, 680, 1, 0)
    Case $spellParam = $arrowToTheKnee
      MouseClick($MOUSE_CLICK_PRIMARY, 1195, 680, 1, 0)
    Case $spellParam = $healingLight
      MouseClick($MOUSE_CLICK_PRIMARY, 1145, 680, 1, 0)
    Case Else
      CastSpell(Random(0, 5, 1))
  EndSelect
EndFunc   ;==>CastSpell

MsgBox($MB_ICONINFORMATION, $appName, "Starting. Waiting for Tap Heroes to become active...")

Local $hWnd = WinWaitActive("[TITLE:Tap Heroes; CLASS:Mf2MainClassTh;]")

Sleep(3000)
MouseClick($MOUSE_CLICK_PRIMARY, 960, 540, 1) ;~ Start the game
Sleep(1000)
MouseClick($MOUSE_CLICK_PRIMARY, 960, 580, 1) ;~ Close welcome message

;~ MouseMove(1055, 600) ;~ Attack monster
;~ MouseMove(1170, 530) ;~ Next level button

;~ MouseMove(880, 680) ;~ Attack upgrade
;~ MouseMove(960, 680) ;~ Heal upgrade
;~ MouseMove(1040, 680) ;~ Critical upgrade

;~ MouseMove(820, 680) ;~ Great Tide spell
;~ MouseMove(770, 680) ;~ Gold Rush spell
;~ MouseMove(720, 680) ;~ Hand of God spell

;~ MouseMove(1195, 680) ;~ Arrow to the Knee
;~ MouseMove(1145, 680) ;~ Healing Light
;~ MouseMove(1090, 680) ;~ Battle Cry

Sleep(1000)

Local $collectGoldCounter = 0
Local $nextLevelCounter = 0
Local $upgradeCounter = 0
Local $spellCounter = 0

For $clickCount = 1 To ($clicks - 1) Step 1

  $collectGoldCounter += 1
  $nextLevelCounter += 1
  $upgradeCounter +=1
  $spellCounter += 1

  If _IsPressed($escapeKeyCode, $hDLL) Then
    MsgBox($MB_SYSTEMMODAL, $appName, "The Esc Key was pressed, therefore we will close the application.")
    ExitLoop
  EndIf

  If ($doCollectGold == true And $collectGoldCounter >= $collectGoldMark) Then
    $collectGoldCounter = 0
    CollectGold()
  EndIf

  If ($doGoNextLevel == true And $nextLevelCounter >= $nextLevelMark) Then
    $nextLevelCounter = 0
    GotoNextLevel()
  EndIf

  If ($doUpgrade == true And $upgradeCounter >= $upgradeMark) Then
    $upgradeCounter = 0
    Upgrade(Random(0, 2, 1))
  EndIf

  If ($doCastSpells == true And $spellCounter >= $spellMark) Then
    $spellCounter = 0
    CastSpell(Random(0, 5, 1))
  EndIf

  AttackMonster()
Next

MsgBox($MB_SYSTEMMODAL, $appName, "You have clicked " & $clickCount & " times!")
Local $response = MsgBox($MB_YESNO, $appName, "Close 'Tap Heroes' now?")
If ($IDYES == $response) Then
  WinClose($hWnd)
EndIf
